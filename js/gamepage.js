document.getElementById("runbtn").addEventListener("click", func);

var userInputValue;
var userOutputResult;

function func() {

    userInputValue = printUserInput().toString();

    inputUsedInTestCases = 'import java.util.*; public class testCases { private static int[] userTest0 = twoSum(new int[] {2,7,11,15}, 9); private static int[] userTest1 = twoSum(new int[] {3,2,4}, 6); private static int[] userTest2 = twoSum(new int[] {3,3}, 6); private static int[] expTest0 = new int[] {0,1}; private static int[] expTest1 = new int[] {1,2}; private static int[] expTest2 = new int[] {0,1}; public static void main(String[] args) { String[] boolArray = new String[3]; if (test0()) { boolArray[0] = "Test Case 1: Pass"; } else { boolArray[0] = "Test 1 - twoSum([2,7,11,15], 9) - Fail. Expected: " + Arrays.toString(expTest0) + "; Output: " + Arrays.toString(userTest0); } if (test1()) { boolArray[1] = "Test Case 2: Pass"; } else { boolArray[1] = "Test 2 - twoSum([3,2,4], 6) - Fail. Expected: " + Arrays.toString(expTest1) + "; Output: " + Arrays.toString(userTest1); } if (test2()) { boolArray[2] = "Test Case 3: Pass"; } else { boolArray[2] = "Test 3 - Fail"; } for (int index = 0; index < boolArray.length; index++) { System.out.println(boolArray[index]); } }' + userInputValue + 'public static boolean test0() { return Arrays.equals(userTest0, expTest0); } public static boolean test1() { return Arrays.equals(userTest1, expTest1); } public static boolean test2() { return Arrays.equals(userTest2, expTest2); } }';

    //alert(userInputValue);
    //public class MyClass{public static void main(String args[]) {System.out.println(10);}}
    //console.log(userInputValue);
    //alert(userInputValue);

    const data = { username: 'example' };
    fetch('http://localhost:3000/', {
        method: 'POST', // or 'PUT'
        headers: {
      'Content-Type': 'application/json',
        },
        body: JSON.stringify({val: inputUsedInTestCases})
    }

    )
    .then(response => response.json())
    .then(data => {
        console.log('Success:', data);
        userOutputResult = data.output;
        userOutputResult = userOutputResult.replace(/\n/g, "<br/><br/>");
        document.getElementById("console").innerHTML = "<div>"+ userOutputResult +"</div>"
        //document.getElementById("console").textContent = "> " + userOutputResult;
        //alert(data);
    })
    .catch((error) => {
        console.error('Error:', error);
        alert(error);
    });
}

function playerIncrease() {
    // Change the variable to modify the speed of the number increasing from 0 to (ms)
    let SPEED = 40;
    // Retrieve the percentage value
    let limit = parseInt(document.getElementById("value1").innerHTML, 10);

    for(let i = 0; i <= limit; i++) {
        setTimeout(function () {
            document.getElementById("value1").innerHTML = i + "/10";
        }, SPEED * i * 10);
    }
}

function opponentIncrease() {
    // Change the variable to modify the speed of the number increasing from 0 to (ms)
    let SPEED = 40;
    // Retrieve the percentage value
    let limit = parseInt(document.getElementById("value2").innerHTML, 10);

    for(let i = 0; i <= limit; i++) {
        setTimeout(function () {
            document.getElementById("value2").innerHTML = i + "/10";
        }, SPEED * i * 10);
    }
}

playerIncrease();
opponentIncrease();
