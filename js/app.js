const fetch = require('cross-fetch');
const express = require('express');
const app = express();
const cors = require("cors");
app.use(cors())
const port = 3000;

app.get('/', function (req, res) {
    fetch('https://api.jdoodle.com/v1/execute', {
        //mode: 'cors',
        //credentials: 'include',
        method: 'POST', // or 'PUT'
        referrerPolicy: "origin",
        headers: {
            'Content-Type': 'application/json',
            'clientId': '8856b3a1db044e771794a4f64ff6dc65',
            'clientSecret': '50532c6ec99f9093645f4f8fa9e692f8834e3f471b80646e63f184c4b15c849',
            'script': 'public class MyClass{public static void main(String args[]) {System.out.println(8);}}',
            //'stdin': '',
            'language': 'java',
            'versionIndex': '0',
            //'Access-Control-Allow-Origin': '*',
        },
        //body: JSON.stringify(data),
    }).then(data => res.json(data))
});
app.post('/', function(req, res) {
    console.log(req);
    fetch('https://api.jdoodle.com/v1/execute', {
        //mode: 'cors',
        //credentials: 'include',
        method: 'POST', // or 'PUT'
        referrerPolicy: "origin",
        headers: {
            'Content-Type': 'application/json',
            'clientId': '8856b3a1db044e771794a4f64ff6dc65',
            'clientSecret': '50532c6ec99f9093645f4f8fa9e692f8834e3f471b80646e63f184c4b15c849',
            'script': 'public class MyClass{public static void main(String args[]) {System.out.println(8);}}',
            //'stdin': '',
            'language': 'java',
            'versionIndex': '0',
            //'Access-Control-Allow-Origin': '*',
        },
        json: {
            script : ""+req,
            language: "java",
            versionIndex: "0",
            clientId: "8856b3a1db044e771794a4f64ff6dc65",
            clientSecret:"50532c6ec99f9093645f4f8fa9e692f8834e3f471b80646e63f184c4b15c849"
        },
    }).then(data => res.json(data))
});

app.listen(port, function () {
    console.log(`Node Server is running on port: ${port}!`)
});

